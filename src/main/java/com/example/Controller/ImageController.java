package com.example.Controller;

import com.example.Model.Image;
import com.example.Repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/v1/api/image")
public class ImageController {
//    Specified method
    private  final ImageRepository imageRepository;


    @Autowired
    public ImageController(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }
    @Autowired
    public void saveAccountProfile(@RequestBody Image image){
        imageRepository.save(image);
    }
    @GetMapping("/imageLink/{imageId}")
    public String gethttpImageLink(@PathVariable("imageId") String imageId) {
        String baseUrl = "http://example.com/images/"; // Replace with your base URL
        String imagePath = "path/to/images/" + imageId + ".jpg"; // Replace with your image path and format

        // Build the complete image URI
        String imageUri = UriComponentsBuilder.fromHttpUrl(baseUrl)
                .path(imagePath)
                .toUriString();

        // Generate the HTTP image link
        String imageLink = "<img src=\"" + imageUri + "\" alt=\"Image\">";

        return imageLink;
    }

}
