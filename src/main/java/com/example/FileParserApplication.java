package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.IOException;

@SpringBootApplication
@EnableAutoConfiguration()
@ComponentScan(basePackages = "com.example.Model")
public class FileParserApplication {
	public static void main(String[] args) throws IOException {
		SpringApplication.run(FileParserApplication.class, args);

		FileParserApplication fileParserApplication = new FileParserApplication(new JdbcTemplate());
		fileParserApplication.createImageDataTable();

	}


	private final JdbcTemplate jdbcTemplate;

	public FileParserApplication(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void createImageDataTable() {
		String query = "CREATE TABLE IF NOT EXISTS Image ("
				+ "id INT AUTO_INCREMENT PRIMARY KEY,"
				+ "name VARCHAR(255) NOT NULL,"
				+ "surname VARCHAR(255) NOT NULL,"
				+ "httpImageLink VARCHAR(255) NOT NULL"
				+ ")";

		jdbcTemplate.execute(query);
	}

}
