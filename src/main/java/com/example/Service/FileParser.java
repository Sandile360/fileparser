package com.example.Service;

import com.example.Model.Image;
import com.example.Repository.ImageRepository;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.util.Base64;
@Service
public class FileParser {

    @Autowired
    private final ImageRepository imageRepository;

    public FileParser(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    void parseCSV(MultipartFile file) throws IOException {
//        Intended to parse data from a csv file and store the data to h2 database
        try (CSVReader csvReader = new CSVReader(new InputStreamReader(file.getInputStream()))) {
            String[] line;
            while ((line = csvReader.readNext()) != null) {
                Image imageData = new Image();
                imageData.setName(line[0]);
                imageData.setSurname(line[1]);
                imageData.setHttpImageLink(line[2]);

                // Map the CSV data to your entity fields and set their values accordingly
                imageRepository.save(imageData);
            }
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        }
    }

    BufferedImage convertCSVDataToImage(String csvData) throws IOException{

        // Parse CSV data
        StringReader reader = new StringReader(csvData);
        CSVParser records = CSVFormat.DEFAULT.parse(reader);

        // Create image
        BufferedImage image = new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
        // ... Customize the image as needed based on the CSV data

        // Convert image to byte array
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image, "png", outputStream);
        byte[] imageBytes = outputStream.toByteArray();

        // Encode byte array to Base64 image data
        String base64ImageData = Base64.getEncoder().encodeToString(imageBytes);

        // Convert Base64 image data to BufferedImage
        byte[] decodedImageBytes = Base64.getDecoder().decode(base64ImageData);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(decodedImageBytes);
        return ImageIO.read(inputStream);
    }

    URI createImageLink(File fileImage) {


        return null;
    }


}
